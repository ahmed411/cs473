%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/* external function prototypes */
extern int yylex();
extern int initLex(int ,  char **);
 
    

/* external global variables */

extern int		yydebug;
extern int		yylineno;


/* function prototypes */ 
void	yyerror(const char *);

/* global variables */

%}

/* YYSTYPE */

/* terminals */
/* Start adding token names here */
/* Your token names must match Project 1 */
/* The file cmparser.tab.h was gets generated here */

%token TOK_ELSE 
%token TOK_IF
%token TOK_RETURN
%token TOK_VOID	
%token TOK_INT
%token TOK_WHILE
%token TOK_ID
%token TOK_NUM
%token TOK_PLUS
%token TOK_MINUS
%token TOK_MULT
%token TOK_DIV
%token TOK_LT
%token TOK_LE
%token TOK_GT
%token TOK_GE
%token TOK_EQ
%token TOK_NE
%token TOK_ASSIGN
%token TOK_SEMI	
%token TOK_COMMA
%token TOK_LPAREN
%token TOK_RPAREN
%token TOK_LSQ	
%token TOK_RSQ	
%token TOK_LBRACE
%token TOK_RBRACE
%token TOK_ERROR


/* associativity and precedence */
/* specify operator precedence (taken care of by grammar) and associatity here --uncomment */

%nonassoc "then"
%nonassoc TOK_ELSE
%right TOK_EQ
%left TOK_ASSIGN TOK_NE
%nonassoc TOK_LT TOK_LE TOK_GT TOK_GE
%left TOK_MINUS TOK_PLUS
%right TOK_DIV TOK_MUL
%nonassoc error

/* Begin your grammar specification here */
%%


start	    : declaration
; 


declaration : variableDeclaration declaration
	    | functionDeclaration declaration
            | functionDeclaration
;

variableDeclaration : typeSpecifier TOK_ID TOK_SEMI
                    | typeSpecifier TOK_ID TOK_LSQ expression TOK_RSQ TOK_SEMI
;

typeSpecifier : TOK_INT
              | TOK_VOID
;

functionDeclaration : typeSpecifier TOK_ID TOK_LPAREN params TOK_RPAREN compoundStatement
;

params : paramList
       | TOK_VOID
;

paramList : paramList TOK_COMMA param
          | param
;

param : typeSpecifier TOK_ID
      | typeSpecifier TOK_ID TOK_LSQ TOK_RSQ
;

compoundStatement : TOK_LBRACE localDeclaration statementList TOK_RBRACE
;

localDeclaration : localDeclaration variableDeclaration 
                 |
;

statementList : statementList statement
              | 
;

statement : expressionStatement
          | compoundStatement
          | selectionStatement
          | iterationStatement
          | returnStatement
;

expressionStatement : expression TOK_SEMI
                    | TOK_SEMI
;

selectionStatement : TOK_IF TOK_LPAREN expression TOK_RPAREN statement %prec "then"
                   | TOK_IF TOK_LPAREN expression TOK_RPAREN statement TOK_ELSE statement
;

iterationStatement : TOK_WHILE TOK_LPAREN expression TOK_RPAREN statement
;

returnStatement : TOK_RETURN expression TOK_SEMI
                | TOK_RETURN TOK_SEMI
;

expression : variable TOK_ASSIGN expression 
           | simpleExpression
;

variable : TOK_ID 
         | TOK_ID TOK_LSQ expression TOK_RSQ
;

simpleExpression : additiveExpression relativeOperators additiveExpression 
                 | additiveExpression
;

relativeOperators : TOK_LT
                  | TOK_LE
                  | TOK_GT
                  | TOK_GE
                  | TOK_EQ
                  | TOK_NE
;

additiveExpression : additiveExpression addOperator term
                   | term
;

addOperator : TOK_PLUS
            | TOK_MINUS
;

term : term multOperator factor
     | factor
;

multOperator : TOK_MULT
             | TOK_DIV
;

factor : TOK_LPAREN expression TOK_RPAREN
       | variable
       | call
       | TOK_NUM
;

call : TOK_ID TOK_LPAREN arguments TOK_RPAREN
;

arguments : argumentsList 
          |
;

argumentsList : argumentsList TOK_COMMA expression 
              | expression
;


%%
void yyerror (char const *s) {
       fprintf (stderr, "Line %d: %s\n", yylineno, s);
}

int main(int argc, char **argv){

	initLex(argc,argv);

#ifdef YYLLEXER
   while (gettok() !=0) ; //gettok returns 0 on EOF
    return;
#else
    yyparse();
    
#endif
    
} 
