%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ast.h"
#include "symbolTable.h"
#include "util.h"


/* other external function prototypes */
extern int yylex();
extern int initLex(int ,  char **);
 
    
/* external global variables */

extern int		yydebug;
extern int		yylineno;
extern SymbolTableStackEntryPtr symbolStackTop;
extern int scopeDepth;

/* function prototypes */ 
void yyerror(const char *);
void var_Dec(TypePtr, char *, int);
void array_Dec (TypePtr, char *, int, int);
AstNodePtr new_AstNode (NodeKind);
AstNodePtr join_Siblings(AstNodePtr, AstNodePtr);
AstNodePtr new_Method(TypePtr, char*, AstNodePtr, AstNodePtr, int);
AstNodePtr new_Param(TypePtr, char*, int);
AstNodePtr new_ParamList(AstNodePtr, AstNodePtr);
AstNodePtr new_ArgList(AstNodePtr, AstNodePtr);
AstNodePtr new_ExpTree (AstNodePtr, AstNodePtr, ExpKind, int);
AstNodePtr new_CallExpr (char*, AstNodePtr, int);
AstNodePtr new_VarExpr (char*, int);
AstNodePtr new_ArrayExpr (char*, AstNodePtr, int);
AstNodePtr new_ExprStmt(AstNodePtr, int);
AstNodePtr new_CmpndStmt (AstNodePtr, AstNodePtr, int);
AstNodePtr new_SelectStmt (AstNodePtr, AstNodePtr, AstNodePtr, int);
AstNodePtr new_IterStmt (AstNodePtr, AstNodePtr, int);
AstNodePtr new_ReturnStmt (AstNodePtr, int);
AstNodePtr new_Num (int, int);

/* global variables */
AstNodePtr  program;

%}

/* YYSTYPE */
%union
{
    AstNodePtr nodePtr;
    int        iVal;
    char      *cVal;
    Type      *type;
}

    

/* terminals */

%token TOK_ELSE TOK_IF TOK_RETURN TOK_VOID TOK_INT TOK_WHILE 
%token TOK_PLUS TOK_MINUS TOK_MULT TOK_DIV TOK_LT TOK_LE TOK_GT TOK_GE TOK_EQ TOK_NE TOK_ASSIGN TOK_SEMI TOK_COMMA
%token TOK_LPAREN TOK_RPAREN TOK_LSQ TOK_RSQ TOK_LBRACE TOK_RBRACE TOK_ERROR
%token <cVal> TOK_ID 
%token <iVal> TOK_NUM

%type <nodePtr> Declarations Functions Var_Declaration Local_Declarations
%type <type> Type_Specifier 
%type <nodePtr> Compound_Stmt Statements Statement Fun_Declaration Params Param_List Param
%type <nodePtr> Expr_Statement Selection_Stmt Iteration_Stmt Return_Stmt Term
%type <nodePtr> Expression Simple_Expression Additive_Expression Factor Var Call
%type <nodePtr> Args Args_List

/* associativity and precedence */
%nonassoc TOK_IF
%nonassoc TOK_ELSE
%right TOK_ASSIGN
%left TOK_EQ TOK_NE
%nonassoc TOK_LT TOK_GT	TOK_LE TOK_GE
%left TOK_PLUS TOK_SUB
%left TOK_MULT TOK_DIV 
%nonassoc error

%%


Start	                :               Declarations {  }
;

Declarations            :               Functions { program = $1; }
                        |               Var_Declaration Declarations { }
;

Functions               :               Fun_Declaration { $$ = $1; }
                        |               Fun_Declaration Functions { $$ = join_Siblings($1, $2); }
;

Var_Declaration 	: 		Type_Specifier TOK_ID TOK_SEMI { var_Dec($1, $2, yylineno); $$ = NULL; }
                        | 		Type_Specifier TOK_ID TOK_LSQ TOK_NUM TOK_RSQ TOK_SEMI { array_Dec($1, $2, $4, yylineno); $$ = NULL; }
;

Fun_Declaration         :               Type_Specifier TOK_ID TOK_LPAREN { enterScope(); }
                                        Params TOK_RPAREN Compound_Stmt { $$ = new_Method($1, $2, $5, $7, yylineno);  leaveScope(); }
;

Params                  :               Param_List { $$ = $1; }
                        |               TOK_VOID { $$ = NULL; }
;

Param_List              :               Param_List TOK_COMMA Param { $$ = new_ParamList($1, $3); }
                        |               Param { $$ = new_ParamList(NULL, $1); }
;

Param 			: 		Type_Specifier TOK_ID { $$ = new_Param($1, $2, yylineno); }
                        | 		Type_Specifier TOK_ID TOK_LSQ TOK_RSQ { $$ = new_Param(new_type(ARRAY), $2, yylineno); }
;

Type_Specifier		: 		TOK_INT { $$ = new_type(INT); }
                        | 		TOK_VOID { $$ = new_type(VOID); }
;

Compound_Stmt           :               TOK_LBRACE { enterScope(); } Statements TOK_RBRACE { $$ = new_CmpndStmt(NULL, $3, yylineno); leaveScope(); }
                        |               TOK_LBRACE { enterScope(); } Local_Declarations Statements TOK_RBRACE { $$ = new_CmpndStmt($3, $4, yylineno); leaveScope(); }
;

Local_Declarations      :               Var_Declaration Local_Declarations { $$ = $1 }
                        |               Var_Declaration { $$ = $1; }
;

Statements              :               Statement Statements { $$ = join_Siblings ($1, $2); }
                        |               { $$ = NULL; }
;

Statement               :               Expr_Statement  { $$ = $1; }
               	        |               Compound_Stmt { $$ = $1; }
	                |               Selection_Stmt { $$ = $1; }
  	                |               Iteration_Stmt { $$ = $1; }
	                |               Return_Stmt { $$ = $1; }
;

Expr_Statement          :               Expression TOK_SEMI { $$ = new_ExprStmt($1, yylineno); }
                        |               TOK_SEMI { $$ = NULL; }
;

Selection_Stmt          :               TOK_IF TOK_LPAREN Expression TOK_RPAREN Statement %prec TOK_IF { $$ = new_SelectStmt($3, $5, NULL, yylineno); }
                        |               TOK_IF TOK_LPAREN Expression TOK_RPAREN Statement TOK_ELSE Statement { $$ = new_SelectStmt($3, $5, $7, yylineno) ;}
;

Iteration_Stmt          :               TOK_WHILE TOK_LPAREN Expression TOK_RPAREN Statement { $$ = new_IterStmt($3, $5, yylineno); }
;

Return_Stmt             :               TOK_RETURN Expression TOK_SEMI { $$ = new_ReturnStmt ($2, yylineno); }
                        |               TOK_RETURN TOK_SEMI { $$ = new_ReturnStmt (NULL, yylineno); }
;

Expression              :               Var TOK_ASSIGN Expression { $$ = new_ExpTree($1, $3, ASSI_EXP, yylineno); }
                        |               Simple_Expression { $$ = $1; }
;

Var                     :               TOK_ID { $$ = new_VarExpr($1, yylineno); }
                        |               TOK_ID TOK_LSQ Expression TOK_RSQ { $$ = new_ArrayExpr($1, $3, yylineno); }
;

Simple_Expression       :               Additive_Expression TOK_GT Additive_Expression { $$ = new_ExpTree($1, $3, GT_EXP, yylineno); }
                        |               Additive_Expression TOK_LT Additive_Expression { $$ = new_ExpTree($1, $3, LT_EXP, yylineno); }
                        |               Additive_Expression TOK_GE Additive_Expression { $$ = new_ExpTree($1, $3, GE_EXP, yylineno); }
                        |               Additive_Expression TOK_LE Additive_Expression { $$ = new_ExpTree($1, $3, LE_EXP, yylineno); }
                        |               Additive_Expression TOK_EQ Additive_Expression { $$ = new_ExpTree($1, $3, EQ_EXP, yylineno); }
                        |               Additive_Expression TOK_NE Additive_Expression { $$ = new_ExpTree($1, $3, NE_EXP, yylineno); }
                        |               Additive_Expression { $$ = $1; }
;

Additive_Expression     :               Additive_Expression TOK_PLUS Term { $$ = new_ExpTree($1, $3, ADD_EXP, yylineno); }
                        |               Additive_Expression TOK_MINUS Term { $$ = new_ExpTree($1, $3, SUB_EXP, yylineno); }
                        |               Term { $$ = $1; }
;

Term                    :               Term TOK_MULT Factor { $$ = new_ExpTree($1, $3, MULT_EXP, yylineno); }
                        |               Term TOK_DIV Factor { $$ = new_ExpTree($1, $3, DIV_EXP, yylineno); }
                        |               Factor { $$ = $1; }
;

Factor                  :               TOK_LPAREN Expression TOK_RPAREN { $$ = $2; }
                        |               Var { $$ = $1; }
                        |               Call { $$ = $1; }
                        |               TOK_NUM {  $$ = new_Num($1, yylineno); }
;

Call                    :               TOK_ID TOK_LPAREN Args TOK_RPAREN { $$ = new_CallExpr($1, $3, yylineno); }
;

Args                    :               Args_List { $$ = $1; } 
                        |               { $$ = NULL; }
;

Args_List               :               Args_List TOK_COMMA Expression { $$ = new_ArgList($1, $3); }
                        |               Expression { $$ = $1; }
;

%%

//function to print error
void yyerror (char const *s) {
       fprintf (stderr, "Line %d: %s\n", yylineno, s);
       exit(1);
}

//new AST node
AstNodePtr new_AstNode (NodeKind kind) {
  AstNodePtr node = (AstNodePtr) malloc (sizeof(AstNode));
  if(node == NULL){
    yyerror("Failed to malloc for AstNode");
  }
  node->children[0] = NULL;
  node->children[1] = NULL;
  node->children[2] = NULL;
  node->sibling = NULL;
  node->nKind = kind;
  node->nSymbolPtr = NULL;
  node->nSymbolTabPtr = NULL;
  return node;
}

//variable declaration
void var_Dec(TypePtr type, char * ID, int lineno) {
  ElementPtr elem = symLookup (ID);
  if (elem != NULL) {
    yyerror ("Variable re-declared");
  }
  else {
    elem = symInsert(ID, type, lineno);
  }
}
//array declaration
void array_Dec (TypePtr type, char * ID, int size, int lineno) {
  ElementPtr elem = symLookup (ID);
  if (elem != NULL) {
    yyerror ("Variable re-declared");
  }
  else {
    type->kind = ARRAY;
    elem = symInsert(ID, type, lineno);
    elem->stype->dimension = size;
  }
}

//new AST node for joining Siblings
AstNodePtr join_Siblings(AstNodePtr sibling1, AstNodePtr sibling2) {
  
  AstNodePtr node = sibling1;
  if(sibling1 != NULL) {
    while(node->sibling != NULL)
      node = node->sibling;
    node->sibling = sibling2;
    return sibling1;
  }
  else {
    return sibling2;
  }
}

//new AST node for method
AstNodePtr new_Method(TypePtr typeSpecifier, char* ID, AstNodePtr params, AstNodePtr compSt, int lineno) {

  AstNodePtr node = new_AstNode(METHOD);
  Type* type = new_type(FUNCTION);
  if (symLookup(ID) != NULL) {
    yyerror ("Method previously defined");
  }
  else {
    node->nSymbolPtr = symInsert(ID, type, lineno);
  }
  node->children[0] = params;
  node->children[1] = compSt;
  node->sibling = NULL;
  node->nType = typeSpecifier;
  node->nLinenumber = lineno;
  
  return node;
}

//new AST node for Param
AstNodePtr new_Param(TypePtr typeSpecifier, char* ID, int lineno) {
  AstNodePtr node = new_AstNode(FORMALVAR);
  node->nSymbolPtr = symInsert(ID, typeSpecifier, lineno);
  return node;
}

//new AST node for ParamsList
AstNodePtr new_ParamList(AstNodePtr paramList, AstNodePtr param) {
  AstNodePtr node = paramList;
  if(paramList != NULL) {
    while(node->sibling != NULL)
      node = node->sibling;
    node->sibling = param;
    return paramList;
  }
  else {
    return param;
  }
}

//new AST node for ArgsList
AstNodePtr new_ArgList(AstNodePtr argList, AstNodePtr expression)
{
  AstNodePtr node = argList;
  while(node->sibling != NULL)
    node = node->sibling;
  node->sibling = expression;
  return argList;
}

//new AST node for Simple Expression
AstNodePtr new_ExpTree (AstNodePtr child1, AstNodePtr child2, ExpKind expType, int lineno) {
  AstNodePtr root = new_ExprNode(expType);
  root->children[0] = child1;
  root->children[1] = child2;
  root->nLinenumber = lineno;
  return root;
}

//new AST node for call expression
AstNodePtr new_CallExpr (char* ID, AstNodePtr args, int lineno) {
  AstNodePtr root = new_ExprNode(CALL_EXP);
  root->children[0] = args;
  root->fname = ID;
  return root;
}

//new AST node for VAR expression
AstNodePtr new_VarExpr (char* ID, int lineno) {
  AstNodePtr root = new_ExprNode(VAR_EXP);
  root->nSymbolPtr = symLookup(ID);
  if (root->nSymbolPtr == NULL) {
    yyerror("Variable not defined previously");
  }
  root->nLinenumber = lineno;
  return root;
}

//new AST node for Array Expression
AstNodePtr new_ArrayExpr (char* ID, AstNodePtr expr, int lineno) {
  AstNodePtr root = new_ExprNode(ARRAY_EXP);
  root->nSymbolPtr = symLookup(ID);
  if (root->nSymbolPtr == NULL) {
    yyerror("Array Variable not defined previously");
  }
  root->nLinenumber = lineno;
  root->children[0] = expr;
  return root;
}

//new AST node for Compound Statement
AstNodePtr new_CmpndStmt (AstNodePtr varDec, AstNodePtr stmts, int lineno) {
  AstNodePtr root = new_StmtNode(COMPOUND_STMT);
  AstNodePtr dummy;
  if (varDec != NULL) {
    dummy = join_Siblings(stmts, varDec);
  }
  else {
    dummy = stmts;
  }
  root->children[0] = dummy;
  root->children[1] = stmts;
  root->nLinenumber = lineno;
  root->nSymbolTabPtr = symbolStackTop->symbolTablePtr;
  return root;
}

//new AST node for expression statement
AstNodePtr new_ExprStmt (AstNodePtr expr, int lineno) {
  AstNodePtr root = new_StmtNode(EXPRESSION_STMT);
  root->children[0] = expr;
  root->nLinenumber = lineno;
  return root;
}

//new AST node for if-else statement
AstNodePtr new_SelectStmt (AstNodePtr expression, AstNodePtr stmt, AstNodePtr elseStmt, int lineno) {
  AstNodePtr root = new_StmtNode(IF_THEN_ELSE_STMT);
  root->children[0] = expression;
  root->children[1] = stmt;
  root->children[2] = elseStmt;
  root->nLinenumber = lineno;
  return root;
}

//new AST node for iteration statement
AstNodePtr new_IterStmt (AstNodePtr expression, AstNodePtr stmt, int lineno) {
  AstNodePtr root = new_StmtNode(WHILE_STMT);
  root->children[0] = expression;
  root->children[1] = stmt;
  root->nLinenumber = lineno;
  return root;
}

//new AST node for return statment
AstNodePtr new_ReturnStmt (AstNodePtr expression, int lineno) {
  AstNodePtr root = new_StmtNode(RETURN_STMT);
  root->children[0] = expression;
  root->nLinenumber = lineno;
  return root;
}

//new AST node for Number constant
AstNodePtr new_Num(int value, int lineno) {
  AstNodePtr root = new_ExprNode(CONST_EXP);
  root->nValue = value;
  root->nLinenumber = lineno;
  return root;
}

int main(int argc, char **argv){

	initLex(argc,argv);
	initSymbolTable();

#ifdef YYLLEXER
   while (gettok() !=0) ; //gettok returns 0 on EOF
    return;
#else
    yyparse();
    //printSymbolTable();
    print_Ast();
    
#endif
    
} 
