#include "symbolTable.h"
#include "ast.h"


extern SymbolTableStackEntryPtr symbolStackTop;

extern int scopeDepth;

//creates a new expression node
AstNodePtr  new_ExprNode(ExpKind kind) 
{
    AstNodePtr node = (AstNodePtr) malloc (sizeof(AstNode));
    if(node == NULL){
         fprintf(stderr, "Failed to malloc for AstNode\n");
    }
    node->children[0] = NULL;
    node->children[1] = NULL;
    node->children[2] = NULL;
    node->sibling = NULL;
    node->nKind = EXPRESSION;
    node->eKind = kind;
    node->nSymbolPtr = NULL;
    node->nSymbolTabPtr = NULL;

    return node;
}

//creates a new statement node
AstNodePtr new_StmtNode(StmtKind kind)
{
    AstNodePtr node = (AstNodePtr) malloc (sizeof(AstNode));
    if(node == NULL){
         fprintf(stderr, "Failed to malloc for AstNode\n");
    }
    node->children[0] = NULL;
    node->children[1] = NULL;
    node->children[2] = NULL;
    node->sibling = NULL;
    node->nKind = STMT;
    node->sKind = kind;
    return node;
}

//creates a new type node for entry into symbol table
Type* new_type(TypeKind kind)
{
    TypePtr typ = (TypePtr) malloc(sizeof(Type));
    typ->kind = kind;
    typ->function = NULL;
    return typ;
}

