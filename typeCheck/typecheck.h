#include <stdlib.h>
#include "symbolTable.h"
#include "ast.h"

//add input output function to the symbol table 
void addInputOutputSymbolTableEntry();

//recursively type checks all the AST nodes starting 
//from program AST root node
void typeCheck_Recursion(AstNodePtr root);

// Starts typechecking the AST  returns 1 on success
//use the global variable program

int typecheck();

// compares two types and returns the resulting type
// from the comparison

Type* type_equiv(Type *, Type *);

// Typechecks a method and returns 1 on success
int typecheck_method(AstNode *);

// Typechecks a statement and returns 1 on success
int typecheck_stmt( AstNode *, TypePtr);

// Type checks a given expression and returns its type
// 

Type *typecheck_expr (AstNode *);

//returns the number of actual paramters of a Call expression AST NODE
int paramsCountNode (AstNodePtr);

//returns the number of formal parameters for a function in symbol table
int paramsCountElement (TypePtr);
