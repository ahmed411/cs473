#include "typecheck.h"


extern SymbolTableStackEntryPtr symbolStackTop;
extern AstNode *program;

//add input output function to the symbol table 
void addInputOutputSymbolTableEntry() {

  //symInsert for input function in Symbol Table
  TypePtr functionType = new_type(FUNCTION);
  ElementPtr inputSymbolTableEntry =  symInsert("input", functionType, 0);
  inputSymbolTableEntry->stype->function = new_type(INT);
  inputSymbolTableEntry->stype->function->function = new_type(VOID);
  //printf("Line %d: %s %d %d %d\n", inputSymbolTableEntry->linenumber,inputSymbolTableEntry->id, inputSymbolTableEntry->stype->kind, inputSymbolTableEntry->stype->function->kind, inputSymbolTableEntry->stype->function->function->kind);

  //symInsert for output function in Symbol Table
  TypePtr functionType1 = new_type(FUNCTION);
  ElementPtr outputSymbolTableEntry =  symInsert("output", functionType1, 0);
  outputSymbolTableEntry->stype->function = new_type(VOID);
  outputSymbolTableEntry->stype->function->function = new_type(INT);
  //printf("Line %d: %s %d %d %d\n", outputSymbolTableEntry->linenumber,outputSymbolTableEntry->id, outputSymbolTableEntry->stype->kind, outputSymbolTableEntry->stype->function->kind, outputSymbolTableEntry->stype->function->function->kind);

}


//should traverse through the entire symbol table and fill in the 
// type of a method in its symbol table using function field.
void methodSymbolTableEntryUpdate()
{
  int i;
  SymbolTableStackEntryPtr SymbolStackEntry = symbolStackTop;
  AstNodePtr tempAstNode = NULL;
  TypePtr currType;
    // rewind
    while (SymbolStackEntry->prev)
        SymbolStackEntry = SymbolStackEntry->prev;

    while(SymbolStackEntry) {
        ElementPtr symelement = SymbolStackEntry->symbolTablePtr->queue;
        while(symelement) {
          if (symelement->stype->kind == 3) {
            tempAstNode = symelement->snode->children[0];
            currType = symelement->stype;
            // printf("Element %s is a function and return type is: %d\n", symelement->id, symelement->stype->function->kind);
            // printf("%d\n", tempType->kind);
            while(tempAstNode) {
              while(currType->function) {
                currType = currType->function;
              }
              currType->function = (TypePtr) malloc(sizeof(Type));
              currType->function = tempAstNode->nSymbolPtr->stype;
              tempAstNode = tempAstNode->sibling;
            }
          }
          symelement = symelement->queue_next; 
        }   
      SymbolStackEntry = SymbolStackEntry->next; 
    }
}


// Starts typechecking the AST  returns 1 on success
//use the global variable program
// 
int typecheck(){
  methodSymbolTableEntryUpdate();
  addInputOutputSymbolTableEntry();
  // printf("===in typecheck\n");
  typecheck_method(program);
  // typeCheck_Recursion(program);
  return 1;
};



// void typeCheck_Recursion(AstNodePtr root) { 
// //printf("print_Ast_Recursion: root = %d\n", root);
//    /*
//     * End the recursion
//     */
//   if(root == NULL)
//     return;
     
//   switch(root->nKind) {
//   case METHOD :
//   //printf("root->nKind = METHOD\n");   
//       // printType(root->nType, 0);
//       // printf(" %s(", root->nSymbolPtr->id);
//       // if(root->children[0] == NULL)
//       //    printf("void");
//       // else
//       //    print_Ast_Recursion(root->children[0]); // print the parameters of the method
//       // printf(")\n");
//       // print_Ast_Recursion(root->children[1]); // print the body of the method
//       // printf("\n");
//       // print_Ast_Recursion(root->sibling); // print the next method

//     printf("typechecking %s method recursively, first the body\n", root->nSymbolPtr->id);
//     typeCheck_Recursion(root->children[1]); //typecheck the body of the method
//     printf("typechecking the siblings of method %s \n", root->nSymbolPtr->id);
//     typeCheck_Recursion(root->sibling);

//   break;
//   case FORMALVAR :
//   //printf("root->nKind = FORMALVAR\n");   
//     printType(root->nSymbolPtr->stype, 0); // print the type
//     printf(" %s", root->nSymbolPtr->id); // print the name of the variable
//     if(root->nSymbolPtr->stype->kind == ARRAY)
//        printf("[]");
//     /*
//      * Print the next parameter if there's one
//      */
//     if(root->sibling != NULL) {
//        printf(", ");
//        print_Ast_Recursion(root->sibling);
//     }
//   break;
//   case STMT :
//     printf("===STATEMENT\n");   
//     typecheck_stmt(root);
//   break;
//   case EXPRESSION :
//   //printf("root->nKind = EXPRESSION\n");   
//     typecheck_expr(root); // I don't think it ever gets here
//   break;
//   }
// }




// Typechecks a method and returns 1 on success
int typecheck_method(AstNodePtr method){
  if(method == NULL)
    return 1;
  AstNodePtr node, node1;
  TypePtr returnType = method->nSymbolPtr->stype->function;
  node = method->children[1];
  while (node) {
    if (typecheck_stmt(node, returnType)) {
      // if (node->sKind == 2){
      //   printf("return statment\n");
      // }
      node = node->sibling;
    }
    else {  
      exit(0);
    }
  }
  // printf("typechecking the siblings of method %s \n", method->nSymbolPtr->id);
  typecheck_method(method->sibling);
  // if (typecheck_stmt(method->children[1])) {
  //   printf("typechecking the siblings of method %s \n", method->nSymbolPtr->id);
  //   typecheck_method(method->sibling);
  // }
}

// Typechecks a statement and returns 1 on success
int typecheck_stmt( AstNodePtr stmt, TypePtr returnType){
  AstNodePtr node;
  if(stmt == NULL) {
    return 0;
  }
  // printf("STMT_KIND= %d\n", stmt->sKind);
  switch(stmt->sKind) {
    case IF_THEN_ELSE_STMT :
      //typechecks the expression condition for if
      if (typeInt(typecheck_expr(stmt->children[0])) == 1) {
        //typechecks the statement for if condition
        if (typecheck_stmt(stmt->children[1], returnType)) {
          //checks if there is an else condition or not?
          if (stmt->children[2]) {
            if (typecheck_stmt(stmt->children[2], returnType)){
              typecheck_stmt(stmt->sibling, returnType);
              return 1;
            }
            else {
              printf("Line %d: ", stmt->nLinenumber);
              print_Statement(stmt);  
              printf(" is not type safe.\n");
              exit(0);
              return 0;
            }
          }
          else {
            typecheck_stmt(stmt->sibling, returnType);
            return 1;
          }
        }
      }
      else {
        return 0;
      } 
    break;
    case WHILE_STMT :
      //typechecks the expression condition for while
      if (typeInt(typecheck_expr(stmt->children[0])) == 1) {
        //typechecks the statement for while loop
        if (typecheck_stmt(stmt->children[1], returnType)) { 
          typecheck_stmt(stmt->sibling, returnType);
          return 1;
        }
        else {
          printf("Line %d: ", stmt->nLinenumber);
          print_Statement(stmt);  
          printf(" is not type safe.\n");
          exit(0);
          return 0;
        }
      }
      else {
        return 0;
      }
    break;
    case COMPOUND_STMT :
      if (typecheck_stmt(stmt->children[0], returnType)) {
        typecheck_stmt(stmt->sibling, returnType);
        return 1;
      }
      else if (!stmt->children[0]){
        return 1;
      } 
      else {
        printf("Line %d: ", stmt->nLinenumber);
        print_Statement(stmt);  
        printf(" is not type safe.\n");
        exit(0);
        return 0;
      }
    break;
    case RETURN_STMT :
      if (!stmt->children[0] && typeInt(returnType) == 0) {
        typecheck_stmt(stmt->sibling, returnType);
        return 1;
      }
      else if (stmt->children[0] && type_equiv(typecheck_expr(stmt->children[0]), returnType)) {
        typecheck_stmt(stmt->sibling, returnType);
        return 1;
      }
      else {
        printf("Line %d: ", stmt->nLinenumber);
        print_Statement(stmt);  
        printf(" does not have valid return type.\n");
        exit(0);
        return 0;
      }
      // if (!stmt->sibling) {
      //   printf("end of function reached =========\n");
      // }
      // else {
      //   printf("NO END return =========\n");
      // }

    break;
    case EXPRESSION_STMT :
      if (!typecheck_expr(stmt->children[0])) {
        printf("Line %d: ", stmt->nLinenumber);
        print_Statement(stmt);  
        printf(" is not type safe.\n");
        exit(0);
      }
      else {
        typecheck_stmt(stmt->sibling, returnType);
        return 1;
      } 
    break;
  }  
  // printf("sibling : %d\n", stmt->sibling);   
  // typecheck_stmt(stmt->sibling); // Print the next statement
  // return 1;

}

// Type checks a given expression and returns its type

TypePtr typecheck_expr (AstNodePtr expr){
  if(expr == NULL) {
    return;
  }
  ElementPtr elem;
  int i = 0;
  TypePtr type;
  AstNodePtr nodeAst;
  // printf("EXP_KIND= %d\n", expr->eKind);
  switch(expr->eKind) {
    case VAR_EXP :
        return expr->nSymbolPtr->stype;
    break;
    case ARRAY_EXP :
      //check if the type of the variable is ARRAY
      if (typeInt(expr->nSymbolPtr->stype) == 2) {
        //check if the type of the index is of int type
        if (typeInt(typecheck_expr(expr->children[0])) == 1) {
          return new_type(INT);
        }
        //index is not of int type
        else {
          printf("Line %d: ", expr->nLinenumber);
          print_Expression(expr);  
          printf(" has non-int index.\n");
          exit(0);
        }
      }
      //variable is not of type ARRAY
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operand of invalid type.\n");
        exit(0);
      }
    break;
    case ASSI_EXP :
      //typecheck for a l-value on LHS
      if (expr->children[0]->eKind == VAR_EXP || expr->children[0]->eKind == ARRAY_EXP && typeInt(typecheck_expr(expr->children[0]->children[0])) == 1) {
        //type checks the LHS and RHS
        if (type_equiv(typecheck_expr(expr->children[0]), typecheck_expr(expr->children[1]))) {
          return new_type(INT);
        }
        else {
          printf("Line %d: ", expr->nLinenumber);
          print_Expression(expr);  
          printf(" should have lvalue on LHS.\n");
          exit(0);
        }
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" should have lvalue on LHS.\n");
        exit(0);
      }
    break;
    case ADD_EXP :
      //type checks whether both children are INT types or not
      if (typeInt(typecheck_expr(expr->children[0])) == 1 && typeInt(typecheck_expr(expr->children[1])) == 1) {
        return new_type(INT);
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operand of invalid type. Expected both INT operands\n");
        exit(0);
      }
    break;
    case SUB_EXP :
      //type checks whether both children are INT types or not
      if (typeInt(typecheck_expr(expr->children[0])) == 1 && typeInt(typecheck_expr(expr->children[1])) == 1) {
        return new_type(INT);
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operand of invalid type. Expected both INT operands\n");
        exit(0);
      }
    break;
    case MULT_EXP :
      //type checks whether both children are INT types or not
      if (typeInt(typecheck_expr(expr->children[0])) == 1 && typeInt(typecheck_expr(expr->children[1])) == 1) {
        return new_type(INT);
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operand of invalid type. Expected both INT operands\n");
        exit(0);
      }
    break;
    case DIV_EXP :
      //type checks whether both children are INT types or not
      if (typeInt(typecheck_expr(expr->children[0])) == 1 && typeInt(typecheck_expr(expr->children[1])) == 1) {
        return new_type(INT);
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operand of invalid type. Expected both INT operands\n");
        exit(0);
      }
    break;
    case GT_EXP :
      //type checks whether both children are INT types or not
      if (typeInt(typecheck_expr(expr->children[0])) == 1 && typeInt(typecheck_expr(expr->children[1])) == 1) {
        return new_type(INT);
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operand of invalid type. Expected both INT operands\n");
        exit(0);
      }
    break;
    case LT_EXP :
      //type checks whether both children are INT types or not
      if (typeInt(typecheck_expr(expr->children[0])) == 1 && typeInt(typecheck_expr(expr->children[1])) == 1) {
        return new_type(INT);
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operand of invalid type. Expected both INT operands\n");
        exit(0);
      }
    break;
    case GE_EXP :
      //type checks whether both children are INT types or not
      if (typeInt(typecheck_expr(expr->children[0])) == 1 && typeInt(typecheck_expr(expr->children[1])) == 1) {
          return new_type(INT);
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operand of invalid type. Expected both INT operands\n");
        exit(0);
      }
    break; 
    case LE_EXP :
      //type checks whether both children are INT types or not
      if (typeInt(typecheck_expr(expr->children[0])) == 1 && typeInt(typecheck_expr(expr->children[1])) == 1) {
        return new_type(INT);
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operand of invalid type. Expected both INT operands\n");
        exit(0);
      }
    break;
    case EQ_EXP :
      //type checks the two children are of same types
      if (type_equiv(typecheck_expr(expr->children[0]), typecheck_expr(expr->children[1]))) {
        return new_type(INT);
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operands of different types\n");
        exit(0);
      }
    break;
    case NE_EXP :
      //type checks the two children are of same types
      if (type_equiv(typecheck_expr(expr->children[0]), typecheck_expr(expr->children[1]))) {
        return new_type(INT);
      }
      else {
        printf("Line %d: ", expr->nLinenumber);
        print_Expression(expr);  
        printf(" has operands of different types\n");
        exit(0);
        }
    break;
    case CALL_EXP :
      // check if the function with this name exists or not in ST
      elem = symLookup(expr->fname);
      if (!elem) {
        printf("%s not found\n", expr->fname);
        exit(0);
      }
      //check if an entry in symbol table exist with FUNCTION type
      if (typeInt(elem->stype) == 3) {
        type = elem->stype->function->function;
        nodeAst = expr->children[0];
        //check if the number of formal parameters equal the number 
        //of actual parameters
        if (paramsCountNode(expr->children[0]) == paramsCountElement(elem->stype)){
          //type check for each parameter
          for (i = 0; i < paramsCountElement(elem->stype); i ++) {
            if (type_equiv(typecheck_expr(nodeAst), type)) {
              nodeAst = nodeAst->sibling;
              type = type->function;
            }
            else {
              printf("Line %d: ", expr->nLinenumber);
              print_Expression(expr);  
              printf(" has inconsistent argument types\n");
              exit(0);
            }
          }
          return new_type(elem->stype->function->kind);
        }
        // formal parameter is only void and actual parameter count is zero
        else if (paramsCountNode(expr->children[0]) == 0 && typeInt(type) == 0) {
          return new_type(elem->stype->function->kind);
        }
        else {
          printf("Line %d: ", expr->nLinenumber);
          print_Expression(expr);  
          printf(" CALL_EXP has different number of arguments than previously defined\n");
          exit(0);
        }
      }
      else {
          printf("Line %d: ", expr->nLinenumber);
          print_Expression(expr);  
          printf(" is not a function\n");
          exit(0);
      }
    break;
    case CONST_EXP :
      return new_type(INT);
    break;
   }
}

// compares two types and returns the resulting type
// from the comparison

Type* type_equiv(Type *t1, Type *t2){

  if(t1->kind == t2->kind) {
    return new_type(t1->kind);
  }
  else {
    printf("Types given %s and %s are not equal\n", typeNameConv(t1), typeNameConv(t2));
    return NULL;
  }
}

//returns the number of actual paramters of a Call expression AST NODE
int paramsCountNode (AstNodePtr node) {
  int count = 0;
  while (node) {
    count++;
    node = node->sibling;
  }
  return count;
}

//returns the number of formal paramters of a function in symbol table
int paramsCountElement (TypePtr type) {
  int count = 0;
  type = type->function;
  while (type->function) {
    count++;
    type = type->function;
  }
  return count;
}
