/*******Name: Chaudhary Ahmed Sharif**********
 ************UIN: 662623962*******************
 *********************************************/
#include "symbolTable.h"

// Top should point to the top of the scope stack,
// which is the most recent scope pushed

SymbolTableStackEntryPtr symbolStackTop;

int scopeDepth;

//this function is used to define an ElementPtr object whenever needed
ElementPtr newElementPtr(int key, char *id, int line, int scope, struct type *type) { 
  ElementPtr p = (ElementPtr) malloc(sizeof(Element));
  if (p == NULL) {
    perror("elementEntry malloc failed");
    return NULL;
  }
  p->key = key;
  p->id = id;
  p->linenumber = line;
  p->scope = scope;
  p->stype = type;
  p->ast = NULL;
  p->next = NULL;
  return p;
}

//this function is used to define the SymbolTable for a scope
SymbolTablePtr newSymbolTablePtr() { 
  int i;
  SymbolTablePtr p = (SymbolTablePtr) malloc(sizeof(struct symbolTable));
  if (p == NULL) {
    perror("symbolTableEntry malloc failed");
    return NULL;
  }
  for (i = 0; i < MAXHASHSIZE; i++) {
    p->hashTable[i] = NULL; 
  }
  return p;
}

//this function is used to define the SymbolTableStack entry
SymbolTableStackEntryPtr newSymbolTableStackPtr() { 
  SymbolTableStackEntryPtr p = (SymbolTableStackEntryPtr) malloc(sizeof(SymbolTableStackEntry));
  if (p == NULL) {
    return NULL;
  }
  p->symbolTablePtr = newSymbolTablePtr();
  p->prevScope = NULL;
  return p;
}

/* global function prototypes */

//allocate the global scope entry and symbol table --and set scopeDepth to 0
// The global scope remains till the end of the program
// return 1 on success, 0 on failure (such as a failed malloc)
int	initSymbolTable() 
{
  scopeDepth = 0;
  
  SymbolTableStackEntryPtr symbolTableStack = newSymbolTableStackPtr();
  if (symbolTableStack == NULL) {
    perror("symbolTableStack init malloc failed");
    return 0;
  }
  symbolStackTop = symbolTableStack;
  
  return 1;
}

//function to calculate the sum of ASCII values of a string
int sum(char *s) { return *s == 0 ? 0 : *s + sum(s + 1); }

//function to return the hash value of a string
int hashIt(char *s) { return sum(s) % MAXHASHSIZE; }

//given a hastable, it looks for the symbol in it
ElementPtr hashTSearch (HashTableEntry *hashTable, char *name)
{
  int hashValue = hashIt(name);
  ElementPtr currElementPtr = hashTable[hashValue];
  while(currElementPtr) {
    if(currElementPtr->id == name){
      return currElementPtr;
    }
    else {
      currElementPtr = currElementPtr->next;
    }
  }
  return NULL;
}

// Look up a given entry 
ElementPtr symLookup(char *name)
{
  SymbolTableStackEntryPtr currSymbolTableTop = symbolStackTop;
  if (currSymbolTableTop == NULL) {
    return NULL;
  }
  SymbolTablePtr currSymbolTable = currSymbolTableTop->symbolTablePtr;
  ElementPtr elementFound = NULL;
  while (currSymbolTable) {
    elementFound = hashTSearch (currSymbolTable->hashTable, name);
    if (elementFound){
      return elementFound;
    }
    else {
      if (currSymbolTableTop->prevScope == NULL) {
	return NULL;
      }
      currSymbolTableTop = currSymbolTableTop->prevScope;
      currSymbolTable = currSymbolTableTop->symbolTablePtr;
    }
  }
  return NULL;
}

// Insert an element with a specified type in a particular line number
// initialize the scope depth of the entry from the global var scopeDepth
ElementPtr symInsert(char *name, struct type *type, int line)
{
  if (name == NULL) {
    perror("symbol name is NULL");
    return NULL;
  }
  int hashValue = hashIt(name);
  ElementPtr returnPtr, dummyElement = NULL;
  if (symbolStackTop->symbolTablePtr->hashTable[hashValue] == NULL) {
    symbolStackTop->symbolTablePtr->hashTable[hashValue] = newElementPtr(hashValue, name, line, scopeDepth, type);
    returnPtr = symbolStackTop->symbolTablePtr->hashTable[hashValue];
  }
  else {
    dummyElement = symbolStackTop->symbolTablePtr->hashTable[hashValue];
    while (dummyElement->next != NULL) {
     	dummyElement = dummyElement->next;
    }
    dummyElement->next = newElementPtr(hashValue, name, line, scopeDepth, type); 
    returnPtr = dummyElement->next;
  }
 
  return returnPtr;
}

//push a new entry to the symbol stack
// This should modify the variable top and change the scope depth
// return 1 on success, 0 on failure (such as a failed malloc)
int enterScope()
{
  SymbolTableStackEntryPtr symbolTableStack = newSymbolTableStackPtr();
  if (symbolTableStack == NULL) {
    return 0;
  }
  symbolTableStack->prevScope = symbolStackTop;
  symbolStackTop = symbolTableStack;
  scopeDepth++;
  return 1;
}

// pop an entry off the symbol stack
// This should modify top and change scope depth
void leaveScope()
{
  symbolStackTop = symbolStackTop->prevScope;
  scopeDepth--;
}

// Do not modify this function
void printElement(ElementPtr symelement) {
  if (symelement != NULL) {
    printf("Line %d: %s", symelement->linenumber,symelement->id);
  }
  else printf("Wrong call! symbol table entry NULL");
}

//should traverse through the entire symbol table and print it
// must use the printElement function given above
void printSymbolTable()
{
  int i;
  SymbolTableStackEntryPtr currSymbolTableTop = symbolStackTop;
  if (currSymbolTableTop == NULL) {
    return;
  }
  ElementPtr currElement=NULL;
  while(currSymbolTableTop != NULL) {
    for (i = 0; i < MAXHASHSIZE; i++) {
      if (currSymbolTableTop->symbolTablePtr->hashTable[i] !=NULL) {
	currElement = currSymbolTableTop->symbolTablePtr->hashTable[i];
	
	while(currElement->next) {
	  printElement(currElement);
	  currElement = currElement->next;
	}
	printElement(currElement);
      }
    }
    currSymbolTableTop = currSymbolTableTop->prevScope;
  }
}
