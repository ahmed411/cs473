I have learned the following things while doing this homework:
1. Being from a non-CS background (Electrical Engineering), I hadn't came across of theory of automata and regular expressions before this class. So, I spent some extra time to cover these topics along with the lectures given in class by Professor Venkat.
2. The semantics of working with lex.
3. How the input stream is tokenized using lexemes, patterns and tokens.


The most difficult part for me in this project was writing down regular expressions based on the finite state automaton. Specifically, handling C-style multi-line comments was the part that took most of my time. 

In terms of diffculty, I would rate it 6 out of 10. Expecting even harder ones to come though. 